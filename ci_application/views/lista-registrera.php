﻿<h2>Registrera på ny kurs</h2>

<?php
 
echo form_open('kurser/done/');


echo '<select class="selectpicker" name="registrering">';
foreach($nCourses as $course)
{
 echo "<option value='".$course->course_id."'>".$course->course_name."</option>";
}
echo "</select>";
echo '<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-log-in"></span> Registrera</button>';
echo form_close();
?>