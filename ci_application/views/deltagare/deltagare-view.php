<div class="page-header row  text-center">
    <h1>Lista över deltagare i kursen <?= $course->course_name;?></h1>
</div>
<div class="container">
    <p class="row">Max poäng: <?= $max_points; ?>
<table class="table table-striped table-hover table-condensed">
	<thead>
		<tr>
			<th width="25">#</th>
			<th width="200">Name</th>
			<th width="250">E-mail</th>
                        <th width="100">Poäng</th>
			<th width="*"></th>
		</tr>
	</thead>
	<tbody>
<?php
$count=1;
foreach($all_members as $member):
	echo"<tr class='tablehover'><th>".$count."</th>";
	echo"<td><p>".$member->user_name."</p></td>
		<td>".$member->email."</td>";

        $found_user = FALSE;
        foreach($accumulated_points as $acc_user)
        {
            if ($member->user_id === $acc_user->user_id)
            {
                echo "<td>".$acc_user->grade."</td>";
                $found_user = TRUE;
            }
        }
        if (!$found_user)
        {
            echo "<td></td>";
        }

	echo '<td><a href="'
            . site_url('anvandare/andra/'.$course->course_id.'/'.$member->user_id)
            . '" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil"></span></a></td></tr>';
	$count++;
	endforeach;
echo"</table>";
//TODO.   isTeacher, ändra email.
?>
