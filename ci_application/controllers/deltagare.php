<?php
/**
 * This file contains the controller for the participant administration page of the application.
 *
 * PHP version 5
 *
 * @category Controllers
 * @package  jarvis
 * @author   Jonathan Ekelund <jonathan.ekelund1@gmail.com>
 * @license  http://URL Proprietary
 * @link     None.invalid
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @category Controllers
 * @package  Public
 * @author   Projektgruppen Järv
 * @license  http://URL Proprietary
 * @link     None.com
 */
class deltagare extends My_Controller
{

    /**
     *
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('deltagare_model');
    }
	
	public function index()
	{
		$this->auto();
	}
	
	public function auto($course_id = 0)
	{
        $isteacher = $this->permission_model->is_teacher($this->user->user_id,
                                                         $course_id);

        //get the users default course
		$course = $this->_get_course_or_default($course_id, $this->user->user_id);
		
		$navCourses = $this->user_model->get_latest_courses($this->user->user_id);

		 $this->load->view('header',
                          array(
            'course'    => $course,
            'navCourses' => $navCourses,
            'header'     => 'Deltagare',
            'active'     => 2
        ));
		
	$all_members = $this->deltagare_model->get_members($course->course_id);
        $accumulated_points = $this->deltagare_model->get_accumulated_points($course->course_id);
        $is_student = $this->permission_model->is_student($this->session->userdata['user_id'], $course_id);
        $this->load->view('deltagare/deltagare-view',
            array(
                    'isteacher'             => $isteacher,
                    'course'                => $course,
                    'all_members'           => $all_members,
                    'accumulated_points'    => $accumulated_points,
                    'is_student'            => $is_student,
                    'max_points'            =>$this->assignment_model->get_point_sum($course->course_id),
                )
        );
        $this->load->view('footer');
	}
}


// End of file deltagare.php
// Location: ./controllers/deltagare.php